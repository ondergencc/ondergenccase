/*
* Projenin Çalıştırılması Gerekli Programlar
* Adım - 1: Yüklü değil ise Node.js kurulumu.
* https://nodejs.org/en/download/ linkinden nodejs kurulumu yapılmalıdır.
* Adım - 2: Yüklü değil ise Anguar CLI kurulumu
* https://cli.angular.io adresinden Agular CLI kurulumu yapılmalıdır.
* 
* Kurulumlar bittikten sonra Node.js çalıştırılıp projenin olduğu dosya dizinine gidilmelidir.
* Dosya dizininde node_modules dosyalarının kurulumu için npm install --save yazılıp kurulum bitene kadar beklenmelidir.
* Kurulum bittikten sonra ng serve -o (ya da ng serve --open) yazılarak proje çalıştırılır. 
* Çalışan proje default seçili olan browserınızda localhost:4200/ portu ile çalışacaktır.
*/