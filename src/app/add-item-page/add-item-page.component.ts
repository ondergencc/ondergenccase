import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ApiService } from 'src/services/api.service';

@Component({
  selector: 'app-add-item-page',
  templateUrl: './add-item-page.component.html',
  styleUrls: ['./add-item-page.component.scss']
})
export class AddItemPageComponent implements OnInit {

  isFormSubmitted: boolean;
  toastMessageInfo: boolean;
  toastMessageLinkName: string;

  constructor(private service: ApiService) {
    this.isFormSubmitted = false;
    this.toastMessageInfo = false;
  }

  ngOnInit() {
  }

  addLink(addForm: NgForm) {
    if (addForm.valid) {
      const linkName = addForm.form.value.linkName;
      const linkUrl = addForm.form.value.linkUrl;
      this.toastMessageLinkName = linkName;
      this.service.setItem(linkName, linkUrl);
      this.toastMessage();
    } else {
      this.isFormSubmitted = true;
    }
  }

  toastMessage() {
    this.toastMessageInfo = true;
    setTimeout(() => {
      this.toastMessageInfo = false;
    }, 2000);
  }

}
