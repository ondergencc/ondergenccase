import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/services/api.service';
import { ArrayModel } from 'src/models/array.model';

@Component({
  selector: 'app-list-page',
  templateUrl: './list-page.component.html',
  styleUrls: ['./list-page.component.scss']
})
export class ListPageComponent implements OnInit {

  array: ArrayModel[];
  deleteModal: boolean;
  removeItemName: string;
  removeItemId: number;
  toastMessageInfo: boolean;

  constructor(private service: ApiService) { }

  ngOnInit() {
    this.array = this.service.getItem();
    this.array.reverse(); // En son yuklenen item en ustte olmasi icin yapildi.
    this.deleteModal = false;
    this.toastMessageInfo = false;
  }

  openModalPopup(id: number, name: string) {
    this.deleteModal = true;
    this.removeItemName = name;
    this.removeItemId = id;
  }

  closePopup() {
    this.deleteModal = false;
  }

  deleteListItem(id: number) {
    this.array = this.service.getItem();
    const index = this.array.findIndex(element => element.id === id);
    this.service.deleteItem(index);
    this.array = this.service.getItem();
    this.deleteModal = false;
    this.toastMessage();
  }

  toastMessage() {
    this.toastMessageInfo = true;
    setTimeout(() => {
      this.toastMessageInfo = false;
    }, 2000);
  }

  voting(vote: string, id: number) {
    this.array = this.service.getItem();
    if (vote === 'up') {
      const index = this.array.findIndex(element => element.id === id);
      this.array[index].totalVote = this.array[index].totalVote + 1;
      localStorage.setItem('array', JSON.stringify(this.array));
    } else {
      const index = this.array.findIndex(element => element.id === id);
      this.array[index].totalVote = this.array[index].totalVote - 1;
      localStorage.setItem('array', JSON.stringify(this.array));
    }
  }

  orderby(order: string) {
    this.array = this.service.getItem();
    if (order === 'VOTE A - Z') {
      const newArrayOrderList = this.array.sort((a, b) => b.totalVote - a.totalVote);
      localStorage.setItem('array', JSON.stringify(newArrayOrderList));
    } else {
      const newArrayOrderList = this.array.sort((a, b) => a.totalVote - b.totalVote);
      localStorage.setItem('array', JSON.stringify(newArrayOrderList));
    }
  }

}
