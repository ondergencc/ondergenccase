// tslint:disable:variable-name
export interface ArrayModel {
  id: number;
  name: string;
  link: string;
  totalVote: number;
}
