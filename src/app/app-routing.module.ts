import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListPageComponent } from './list-page/list-page.component';
import { AddItemPageComponent } from './add-item-page/add-item-page.component';


const routes: Routes = [
  {
    path: '',
    component: ListPageComponent
  },
  {
    path: 'add-item',
    component: AddItemPageComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
