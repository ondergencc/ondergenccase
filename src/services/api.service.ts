import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  // testArray = [
  //   {
  //     id: 0,
  //     name: 'Hacker News',
  //     link: 'https://news.ycombinator.com/',
  //     totalVote: 5
  //   },
  //   {
  //     id: 1,
  //     name: 'Hacker News',
  //     link: 'https://news.ycombinator.com/',
  //     totalVote: 3
  //   },
  //   {
  //     id: 2,
  //     name: 'Hacker News',
  //     link: 'https://news.ycombinator.com/',
  //     totalVote: 9
  //   },
  //   {
  //     id: 3,
  //     name: 'Hacker News',
  //     link: 'https://news.ycombinator.com/',
  //     totalVote: 20
  //   },
  //   {
  //     id: 4,
  //     name: 'Hacker News',
  //     link: 'https://news.ycombinator.com/',
  //     totalVote: 1
  //   },
  // ];

  setItemIndex: number;

  constructor() {
    // localStorage.setItem('array', JSON.stringify(this.testArray));
  }

  getItem() {
    if (localStorage.getItem('array')) {
      return JSON.parse(localStorage.getItem('array'));
    } else {
      console.log('Error, listede data yok');
      return [];
    }
  }

  deleteItem(id: number) {
    if (localStorage.getItem('array')) {
      const newArray = JSON.parse(localStorage.getItem('array'));
      newArray.splice(id, 1);
      localStorage.setItem('array', JSON.stringify(newArray));
    }
  }

  setItem(linkName: string, linkUrl: string) {
    const array = JSON.parse(localStorage.getItem('array'));
    const arrayLengthControl = JSON.parse(localStorage.getItem('arrayLengthControl'));
    /**
     * Benzersiz id olusturmak icin 'arrayLengthControl' adinda bir dizi olusturuyorum.
     * 'Array' dizisinde itemlar delete edilse bile 'arrayLengthControl' dizisinde itemlar silinmiyor.
     * Bu dizinin lengthi hic azalmadigi icin yeni id eklerken 'arrayLengthControl.length' seklinde ekliyorum.
     */
    if (!array) {
      this.setItemIndex = 0;
      const firstItem = [
        {
          id: this.setItemIndex,
          name: linkName,
          link: linkUrl,
          totalVote: 0
        }
      ];
      localStorage.setItem('array', JSON.stringify(firstItem));
      localStorage.setItem('arrayLengthControl', JSON.stringify(firstItem));
    } else {
      this.setItemIndex = arrayLengthControl.length;
      const newItem = [...array,
        {
          id: this.setItemIndex,
          name: linkName,
          link: linkUrl,
          totalVote: 0
        }
      ];
      const newItemLength = [...arrayLengthControl,
        {
          id: this.setItemIndex,
          name: linkName,
          link: linkUrl,
          totalVote: 0
        }
      ];
      localStorage.setItem('array', JSON.stringify(newItem));
      localStorage.setItem('arrayLengthControl', JSON.stringify(newItemLength));
    }
  }
}
